const { default: axios } = require('axios');
const bodyParser = require('body-parser');
const express = require('express');
const { fs } = require('file-system');
const { PKPass } = require("passkit-generator");

const app = express();
app.use(bodyParser.json());


function getCertificatesContentsSomehow() {
    return {
        wwdr: fs.readFileSync("./certs/wwdr.pem"),
        signerCert: fs.readFileSync("./certs/signerCert.pem"),
        signerKey: fs.readFileSync("./certs/signerKey.pem"),
        signerKeyPassphrase: "test"
    };
}

app.post('/', async function (req, res) {
    const { userId, userName, barcodeContent } = req.body;
    const { wwdr, signerCert, signerKey, signerKeyPassphrase } = getCertificatesContentsSomehow();

    const now = new Date();
    const day = String(now.getDate()).padStart(2, '0');
    const month = String(now.getMonth() + 1).padStart(2, '0');
    const year = now.getFullYear();

    const pass = await PKPass.from(
        {
            model: "./models/custom.pass",
            certificates: {
                wwdr,
                signerCert,
                signerKey,
                signerKeyPassphrase,
            },
        },
        {
            serialNumber: userId,
            labelColor: "rgb(255, 247, 86)",
            logoText: 'FAHASA Card',
            userInfo: {
                userId: userId,
                userName: userName,
            },
        }
    );

    pass.type = "coupon"; // boardingPass|coupon|eventTicket|storeCard|generic

    pass.headerFields.push(
        {
            key: "header1",
            label: "",
            value: "Thẻ thành viên",
            textAlignment: "PKTextAlignmentCenter",
        },
    );

    pass.primaryFields.push({
        key: "primary",
        label: "Sử dụng thẻ để tích điểm khi mua hàng",
        value: userName
    });

    pass.secondaryFields.push({
        key: "secondary0",
        label: "Ngày tạo thẻ",
        value: `${day}/${month}/${year}`
    });

    // pass.auxiliaryFields.push({
    //     key: "auxiliary 2",
    //     label: "auxiliary label",
    //     value: "auxiliary value"
    // });

    pass.setBarcodes({
        message: barcodeContent + "",
        format: "PKBarcodeFormatQR",
        messageEncoding: "iso-8859-1"
    });

    // const resp = await axios.get("https://cdn0.fahasa.com/skin/frontend/ma_vanese/fahasa/images/fahasa-logo.png", { responseType: "arraybuffer" });
    // const buffer = Buffer.from(resp.data, 'utf-8');

    // pass.addBuffer('thumbnail1.png', buffer);
    // pass.addBuffer('thumbnail2@2x.png', buffer);

    const bufferData = pass.getAsBuffer();
    // fs.writeFileSync('new3.pkpass', bufferData);

    res.send(bufferData);

});





app.listen(3000);